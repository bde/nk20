# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from json import dumps as json_dumps

from django.forms.widgets import DateTimeBaseInput, NumberInput, TextInput, Widget


class AmountInput(NumberInput):
    """
    This input type lets the user type amounts in euros, but forms receive data in cents
    """
    template_name = "note/amount_input.html"

    def format_value(self, value):
        return None if value is None or value == "" else "{:.02f}".format(int(value) / 100, )

    def value_from_datadict(self, data, files, name):
        val = super().value_from_datadict(data, files, name)
        return str(int(100 * float(val))) if val else val


class Autocomplete(TextInput):
    template_name = "autocomplete_model.html"

    def __init__(self, model, resetable=False, attrs=None):
        super().__init__(attrs)

        self.model = model
        self.resetable = resetable
        self.model_pk = None

    class Media:
        """JS/CSS resources needed to render the date-picker calendar."""

        js = ('js/autocomplete_model.js', )

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['resetable'] = self.resetable
        return context

    def format_value(self, value):
        if value:
            self.attrs["model_pk"] = int(value)
            return str(self.model.objects.get(pk=int(value)))
        return ""


class ColorWidget(Widget):
    """
    Pulled from django-colorfield.
    Select a color.
    """
    template_name = 'colorfield/color.html'

    class Media:
        js = [
            'colorfield/jscolor/jscolor.min.js',
            'colorfield/colorfield.js',
        ]

    def format_value(self, value):
        if value is None:
            value = 0xFFFFFF
        return "#{:06X}".format(value)

    def value_from_datadict(self, data, files, name):
        val = super().value_from_datadict(data, files, name)
        return int(val[1:], 16)

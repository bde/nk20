Application Activités
=====================

L'application activités gère les différentes activités liées au BDE. Elle permet entre autres de créer des activités qui
peuvent être diffusées via des calendriers ou la mailing list d'événements. Elle permet aussi de réguler l'accès aux
événements, en s'assurant que leur note est positive. Elle permet enfin de gérer les invité⋅es.

Modèles
-------

L'application comporte 6 modèles : activités, types d'activité, invité⋅es, entrées et transactions d'invitation et les ouvreur⋅ses.

Types d'activité
~~~~~~~~~~~~~~~~

Les activités sont triées par type (pots, soirées de club, ...), et chaque type regroupe diverses informations :

* Nom du type
* Possibilité d'inviter des non-adhérent⋅es (booléen)
* Prix d'invitation (entier, centimes à débiter sur la note de l'hôte)

Activités
~~~~~~~~~

Le modèle d'activité regroupe les informations liées à l'activité même :

* Nom de l'activité
* Description de l'activité
* Créateur⋅rice, personne qui a proposé l'activité
* Club ayant organisé l'activité
* Note sur laquelle verser les crédits d'invitation (peut être nul si non concerné)
* Club invité (généralement le club Kfet)
* Date et heure de début
* Date et heure de fin
* Activité valide (booléen)
* Activité ouverte (booléen)

Entrées
~~~~~~~

Une instance de ce modèle est créé dès que quelqu'un⋅e est inscrit⋅e à l'activité. Sont stockées les informations suivantes :

* Activité concernée (clé étrangère)
* Heure d'entrée
* Note de la personne entrée, ou hôte s'il s'agit d'un⋅e invité⋅e (clé étrangère vers ``NoteUser``)
* Invité⋅e (``OneToOneField`` vers ``Guest``, ``None`` si c'est la personne elle-même qui rentre et non saon invité⋅e)

Il n'est pas possible de créer une entrée si la note est en négatif.

Invité⋅es
~~~~~~~~~

Les adhérent⋅es ont la possibilité d'inviter des ami⋅es. Pour cela, les différentes informations sont enregistrées :

* Activité concernée (clé étrangère)
* Nom de famille
* Prénom
* Note de la personne ayant invité

Certaines contraintes s'appliquent :

* Une personne ne peut pas être invitée plus de 5 fois par an (coupe nom/prénom)
* Un⋅e adhérent⋅e ne peut pas inviter plus de 3 personnes par activité.

Transactions d'invitation
~~~~~~~~~~~~~~~~~~~~~~~~~

On étend le modèle ``Transaction`` de l'application note afin de supporter les transactions d'invitation. Elles ne
comportent qu'un champ supplémentaire, de type ``OneToOneField`` vers ``Guest``.

Ce modèle aurait pu appartenir à l'application ``note``, mais afin de rester modulaire et que l'application ``note``
ne dépende pas de cette application, on procède de cette manière.

Ouvreur⋅ses
~~~~~~~~~~~

Depuis la page d'une activité, il est possible d'ajouter des personnes en tant qu'« ouvreur⋅se ». Cela permet à une
personne sans aucun droit note de pouvoir faire les entrées d'une ``Activity``. Ce rôle n'est valable que pendant que
l'activité est ouverte et sur aucune autre activité. Les ouvreur⋅ses ont aussi accès à l'interface des transactions.

Ce modèle regroupe :
* Activité (clé étrangère)
* Note (clé étrangère)

Graphe
~~~~~~

.. image:: ../_static/img/graphs/activity.svg
   :alt: Graphe de l'application activités

UI
--

Création d'activités
~~~~~~~~~~~~~~~~~~~~

N'importe quel⋅le adhérent⋅e Kfet peut suggérer l'ajout d'une activité via un formulaire.

Gestion des activités
~~~~~~~~~~~~~~~~~~~~~

Les ayant-droit (Res[pot] et respos infos) peuvent valider les activités proposées. Ils peuvent également la modifier
si besoin. Iels peuvent enfin la déclarer ouverte pour lancer l'accès aux entrées.

N'importe qui peut inviter des ami⋅es non adhérent⋅es, tant que les contraintes de nombre (un⋅e adhérent⋅e n'invite pas plus de
trois personnes par activité et une personne ne peut pas être invitée plus de 5 fois par an). L'invitation est
facturée à l'entrée.

Entrées aux soirées
~~~~~~~~~~~~~~~~~~~

L'interface d'entrées est simple et ergonomique. Elle contient un champ de texte. À chaque fois que le champ est
modifié, un tableau est affiché comprenant la liste des invité⋅es et des adhérent⋅es dont le prénom, le nom ou un alias
de la note est acceptée par le texte entré.

En cliquant sur la ligne de la personne qui souhaite rentrer, s'il s'agit d'un⋅e adhérent⋅e, alors la personne est comptée
comme entrée à l'activité, sous réserve que sa note soit positive. S'il s'agit d'un⋅e invité⋅e, alors 3 boutons
apparaîssent, afin de régler la taxe d'invitation : l'un prélève directement depuis la note de l'hôte, les deux autres
permettent un paiement par espèces ou par carte bancaire. En réalité, les deux derniers boutons enregistrent
automatiquement un crédit sur la note de l'hôte, puis une transaction (de type ``GuestTransaction``) est faite depuis
la note de l'hôte vers la note du club organisateur de l'événement.

Si une personne souhaite faire les entrées, il est possible de l'ajouter dans la liste des ouvreur⋅ses depuis la page
de l'activité.

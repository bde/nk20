Application Food
================

L'application ``food`` s'occupe de la traçabilité et permet notamment l'obtention de la liste des allergènes.

Modèles
-------

L'application comporte 5 modèles : Allergen, QRCode, Food, BasicFood, TransformedFood.

Food
~~~~

Ce modèle est un PolymorphicModel et ne sert uniquement à créer BasicFood et TransformedFood.

Le modèle regroupe :

* Nom du produit
* Propriétaire (doit-être un Club)
* Allergènes (ManyToManyField)
* date d'expiration
* a été mangé (booléen)
* est prêt (booléen)

BasicFood
~~~~~~~~~

Les BasicFood correspondent aux produits non modifiés à la Kfet. Ils peuvent correspondre à la fois à des produits achetés en magasin ou à des produits Terre à Terre. Ces produits seront les ingrédients de tous les plats préparés et en conséquent sont les seuls produits à nécessité une saisie manuelle des allergènes.

Le modèle regroupe :

* Type de date (DLC = date limite de consommation, DDM = date de durabilité minimale)
* Date d'arrivée
* Champs de Food

TransformedFood
~~~~~~~~~~~~~~~

Les TransformedFood correspondent aux produits préparés à la Kfet. Ils peuvent être composés de BasicFood et/ou de TransformedFood. La date d'expiration et les allergènes sont automatiquement mis à jour par update (qui doit être exécuté après modification des ingrédients dans les forms par exemple).

Le modèle regroupe :

* Durée de consommation (par défaut 3 jours)
* Ingrédients (ManyToManyField vers Food)
* Date de création
* Champs de Food

Allergen
~~~~~~~~

Le modèle regroupe :

* Nom

QRCode
~~~~~~

Le modèle regroupe :

* nombre (unique, entier positif)
* food (OneToOneField vers Food)

Création de BasicFood
~~~~~~~~~~~~~~~~~~~~~

Un BasicFood a toujours besoin d'un QRCode (depuis l'interface web). Il convient donc de coller le QRCode puis de le scanner et de compléter le formulaire.

Création de TransformedFood
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour créer un TransformedFood, il suffit d'aller dans l'onglet ``traçabilité`` et de cliquer sur l'onglet.

Ajouter un ingrédient
~~~~~~~~~~~~~~~~~~~~~

Un ingrédient a forcément un QRCode. Il convient donc de scanner le QRCode de l'ingrédient et de sélectionner le produit auquel il doit être ajouté.

Remarque : Un produit fini doit avoir un QRCode et inversement.

Terminer un plat
~~~~~~~~~~~~~~~~

Il suffit de coller le QRCode sur le plat, de le scanner et de sélectionner le produit.

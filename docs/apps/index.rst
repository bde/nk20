Applications de la Note Kfet 2020
=================================

.. toctree::
   :maxdepth: 2
   :caption: Applications

   member
   note/index
   activity
   permission
   ../api/index
   registration
   logs
   treasury
   wei

La Note Kfet 2020 est un projet Django, décomposé en applications.
Certaines applications sont développées uniquement pour ce projet, et sont indispensables,
d'autres sont packagées et sont installées comme dépendances.
Enfin, des fonctionnalités annexes ont été rajoutées, mais ne sont pas essentielles au déploiement de la Note Kfet 2020. Leur usage est cependant recommandé.

L'affichage Web utilise le framework Bootstrap4 et quelques morceaux de JavaScript personnalisés.

Applications indispensables
---------------------------

* ``note_kfet`` :
   Application "projet" de django, c'est ici que la configuration de la note est gérée.
* `Member <member>`_ :
   Gestion des profils d'utilisateur⋅rices, des clubs et de leur membres.
* `Note <note>`_ :
   Les notes associées à des utilisateur⋅rices ou des clubs.
* `Activity <activity>`_ :
   La gestion des activités (créations, gestion, entrées, ...)
* `Permission <permission>`_ :
   Backend de droits, limites les pouvoirs des utilisateur⋅rices
* `API <../api>`_ :
   API REST de la note, est notamment utilisée pour rendre la note dynamique
   (notamment la page de conso)
* `Registration <registration>`_ :
   Gestion des inscriptions à la Note Kfet


Applications packagées
----------------------
* ``polymorphic``
    Utiliser pour la création de models polymorphiques (``Note`` et ``Transaction`` notamment) cf `Note <note>`_.

    L'utilisation des models polymorphiques est détaillé sur la documentation du package:
    `<https://django-polymorphic.readthedocs.io/en/stable/>`_

* ``crispy_forms``
    Utiliser pour générer des formulaires avec Bootstrap4
* ``django_tables2``
    utiliser pour afficher des tables de données et les formater, en Python plutôt qu'en HTML.
* ``restframework``
    Base de l'`API <../api>`_.

Applications facultatives
-------------------------
* `Logs <logs>`_
    Enregistre toute les modifications effectuées en base de donnée.
* ``cas-server``
    Serveur central d'authentification, permet d'utiliser son compte de la NoteKfet2020 pour se connecter à d'autre application ayant intégrer un client.
* `Scripts <https://gitlab.crans.org/bde/nk20-scripts>`_
     Ensemble de commande `./manage.py` pour la gestion de la note: import de données, verification d'intégrité, etc...
* `Treasury <treasury>`_ :
    Interface de gestion pour les trésorièr⋅es, émission de factures, remises de chèque, statistiques...
* `WEI <wei>`_ :
    Interface de gestion du WEI.


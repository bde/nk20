# Generated by Django 2.2.28 on 2023-08-23 21:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('member', '0009_auto_20220904_2325'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='promotion',
            field=models.PositiveSmallIntegerField(default=2023, help_text='Year of entry to the school (None if not ENS student)', null=True, verbose_name='promotion'),
        ),
    ]

/**
 * On form submit, create a new friendship
 */
function form_create_trust (e) {
  // Do not submit HTML form
  e.preventDefault()

  // Get data and send to API
  const formData = new FormData(e.target)
  $.getJSON('/api/note/alias/'+formData.get('trusted') + '/',
    function (trusted_alias) {
      if ((trusted_alias.note == formData.get('trusting')))
      {
         addMsg(gettext("You can't add yourself as a friend"), "danger")
         return
      }
      create_trust(formData.get('trusting'), trusted_alias.note)
    }).fail(function (xhr, _textStatus, _error) {
        errMsg(xhr.responseJSON)
    })
}

/**
 * Create a trust between users
 * @param trusting:Integer trusting note id
 * @param trusted:Integer trusted note id
 */
function create_trust(trusting, trusted) {
  $.post('/api/note/trust/', {
      trusting: trusting,
      trusted: trusted,
      csrfmiddlewaretoken: CSRF_TOKEN
  }).done(function () {
  // Reload tables
  $('#trust_table').load(location.pathname + ' #trust_table')
  $('#trusted_table').load(location.pathname + ' #trusted_table')
    addMsg(gettext('Friendship successfully added'), 'success')
  }).fail(function (xhr, _textStatus, _error) {
    errMsg(xhr.responseJSON)
  })
}

/**
 * On click of "delete", delete the trust
 * @param button_id:Integer Trust id to remove
 */
function delete_button (button_id) {
  $.ajax({
    url: '/api/note/trust/' + button_id + '/',
    method: 'DELETE',
    headers: { 'X-CSRFTOKEN': CSRF_TOKEN }
  }).done(function () {
    addMsg(gettext('Friendship successfully deleted'), 'success')
    $('#trust_table').load(location.pathname + ' #trust_table')
    $('#trusted_table').load(location.pathname + ' #trusted_table')
  }).fail(function (xhr, _textStatus, _error) {
    errMsg(xhr.responseJSON)
  })
}

$(document).ready(function () {
  // Attach event
  document.getElementById('form_trust').addEventListener('submit', form_create_trust)
})

# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later


from django.utils.translation import gettext_lazy as _
from django.apps import AppConfig


class FoodkfetConfig(AppConfig):
    name = 'food'
    verbose_name = _('food')

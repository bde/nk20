# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

import django_tables2 as tables
from django_tables2 import A

from .models import TransformedFood


class TransformedFoodTable(tables.Table):
    name = tables.LinkColumn(
        'food:food_view',
        args=[A('pk'), ],
    )

    class Meta:
        model = TransformedFood
        template_name = 'django_tables2/bootstrap4.html'
        fields = ('name', "owner", "allergens", "expiry_date")

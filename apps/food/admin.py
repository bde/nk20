# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin
from django.db import transaction
from note_kfet.admin import admin_site

from .models import Allergen, BasicFood, QRCode, TransformedFood


@admin.register(QRCode, site=admin_site)
class QRCodeAdmin(admin.ModelAdmin):
    pass


@admin.register(BasicFood, site=admin_site)
class BasicFoodAdmin(admin.ModelAdmin):
    @transaction.atomic
    def save_related(self, *args, **kwargs):
        ans = super().save_related(*args, **kwargs)
        args[1].instance.update()
        return ans


@admin.register(TransformedFood, site=admin_site)
class TransformedFoodAdmin(admin.ModelAdmin):
    exclude = ["allergens", "expiry_date"]

    @transaction.atomic
    def save_related(self, request, form, *args, **kwargs):
        super().save_related(request, form, *args, **kwargs)
        form.instance.update()


@admin.register(Allergen, site=admin_site)
class AllergenAdmin(admin.ModelAdmin):
    pass

# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from .views import AllergenViewSet, BasicFoodViewSet, QRCodeViewSet, TransformedFoodViewSet


def register_food_urls(router, path):
    """
    Configure router for Food REST API.
    """
    router.register(path + '/allergen', AllergenViewSet)
    router.register(path + '/basic_food', BasicFoodViewSet)
    router.register(path + '/qrcode', QRCodeViewSet)
    router.register(path + '/transformed_food', TransformedFoodViewSet)

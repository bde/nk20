# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from api.viewsets import ReadProtectedModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter

from .serializers import AllergenSerializer, BasicFoodSerializer, QRCodeSerializer, TransformedFoodSerializer
from ..models import Allergen, BasicFood, QRCode, TransformedFood


class AllergenViewSet(ReadProtectedModelViewSet):
    """
    REST API View set.
    The djangorestframework plugin will get all `Allergen` objects, serialize it to JSON with the given serializer,
    then render it on /api/food/allergen/
    """
    queryset = Allergen.objects.order_by('id')
    serializer_class = AllergenSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['name', ]
    search_fields = ['$name', ]


class BasicFoodViewSet(ReadProtectedModelViewSet):
    """
    REST API View set.
    The djangorestframework plugin will get all `BasicFood` objects, serialize it to JSON with the given serializer,
    then render it on /api/food/basic_food/
    """
    queryset = BasicFood.objects.order_by('id')
    serializer_class = BasicFoodSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['name', ]
    search_fields = ['$name', ]


class QRCodeViewSet(ReadProtectedModelViewSet):
    """
    REST API View set.
    The djangorestframework plugin will get all `QRCode` objects, serialize it to JSON with the given serializer,
    then render it on /api/food/qrcode/
    """
    queryset = QRCode.objects.order_by('id')
    serializer_class = QRCodeSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['qr_code_number', ]
    search_fields = ['$qr_code_number', ]


class TransformedFoodViewSet(ReadProtectedModelViewSet):
    """
    REST API View set.
    The djangorestframework plugin will get all `TransformedFood` objects, serialize it to JSON with the given serializer,
    then render it on /api/food/transformed_food/
    """
    queryset = TransformedFood.objects.order_by('id')
    serializer_class = TransformedFoodSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['name', ]
    search_fields = ['$name', ]

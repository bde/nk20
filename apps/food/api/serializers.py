# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from rest_framework import serializers

from ..models import Allergen, BasicFood, QRCode, TransformedFood


class AllergenSerializer(serializers.ModelSerializer):
    """
    REST API Serializer for Allergen.
    The djangorestframework plugin will analyse the model `Allergen` and parse all fields in the API.
    """

    class Meta:
        model = Allergen
        fields = '__all__'


class BasicFoodSerializer(serializers.ModelSerializer):
    """
    REST API Serializer for BasicFood.
    The djangorestframework plugin will analyse the model `BasicFood` and parse all fields in the API.
    """

    class Meta:
        model = BasicFood
        fields = '__all__'


class QRCodeSerializer(serializers.ModelSerializer):
    """
    REST API Serializer for QRCode.
    The djangorestframework plugin will analyse the model `QRCode` and parse all fields in the API.
    """

    class Meta:
        model = QRCode
        fields = '__all__'


class TransformedFoodSerializer(serializers.ModelSerializer):
    """
    REST API Serializer for TransformedFood.
    The djangorestframework plugin will analyse the model `TransformedFood` and parse all fields in the API.
    """

    class Meta:
        model = TransformedFood
        fields = '__all__'

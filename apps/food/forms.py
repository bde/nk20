# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from random import shuffle

from django import forms
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from member.models import Club
from bootstrap_datepicker_plus.widgets import DateTimePickerInput
from note_kfet.inputs import Autocomplete
from note_kfet.middlewares import get_current_request
from permission.backends import PermissionBackend

from .models import BasicFood, QRCode, TransformedFood


class AddIngredientForms(forms.ModelForm):
    """
    Form for add an ingredient
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['ingredient'].queryset = self.fields['ingredient'].queryset.filter(
            polymorphic_ctype__model='transformedfood',
            is_ready=False,
            is_active=True,
            was_eaten=False,
        )
        # Caution, the logic is inverted here, we flip the logic on saving in AddIngredientView
        self.fields['is_active'].initial = True
        self.fields['is_active'].label = _("Fully used")

    class Meta:
        model = TransformedFood
        fields = ('ingredient', 'is_active')


class BasicFoodForms(forms.ModelForm):
    """
    Form for add non-transformed food
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({"autofocus": "autofocus"})
        self.fields['name'].required = True
        self.fields['owner'].required = True

        # Some example
        self.fields['name'].widget.attrs.update({"placeholder": _("Pasta METRO 5kg")})
        clubs = list(Club.objects.filter(PermissionBackend.filter_queryset(get_current_request(), Club, "change")).all())
        shuffle(clubs)
        self.fields['owner'].widget.attrs["placeholder"] = ", ".join(club.name for club in clubs[:4]) + ", ..."

    class Meta:
        model = BasicFood
        fields = ('name', 'owner', 'date_type', 'expiry_date', 'is_active', 'was_eaten', 'allergens',)
        widgets = {
            "owner": Autocomplete(
                model=Club,
                attrs={"api_url": "/api/members/club/"},
            ),
            'expiry_date': DateTimePickerInput(),
        }


class QRCodeForms(forms.ModelForm):
    """
    Form for create QRCode
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['food_container'].queryset = self.fields['food_container'].queryset.filter(
            is_active=True,
            was_eaten=False,
            polymorphic_ctype__model='transformedfood',
        )

    class Meta:
        model = QRCode
        fields = ('food_container',)


class TransformedFoodForms(forms.ModelForm):
    """
    Form for add transformed food
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({"autofocus": "autofocus"})
        self.fields['name'].required = True
        self.fields['owner'].required = True
        self.fields['creation_date'].required = True
        self.fields['creation_date'].initial = timezone.now
        self.fields['is_active'].initial = True
        self.fields['is_ready'].initial = False
        self.fields['was_eaten'].initial = False

        # Some example
        self.fields['name'].widget.attrs.update({"placeholder": _("Lasagna")})
        clubs = list(Club.objects.filter(PermissionBackend.filter_queryset(get_current_request(), Club, "change")).all())
        shuffle(clubs)
        self.fields['owner'].widget.attrs["placeholder"] = ", ".join(club.name for club in clubs[:4]) + ", ..."

    class Meta:
        model = TransformedFood
        fields = ('name', 'creation_date', 'owner', 'is_active', 'is_ready', 'was_eaten', 'shelf_life')
        widgets = {
            "owner": Autocomplete(
                model=Club,
                attrs={"api_url": "/api/members/club/"},
            ),
            'creation_date': DateTimePickerInput(),
        }

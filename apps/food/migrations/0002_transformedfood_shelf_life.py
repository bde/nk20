# Generated by Django 2.2.28 on 2024-07-06 20:37

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='transformedfood',
            name='shelf_life',
            field=models.DurationField(default=datetime.timedelta(days=3), verbose_name='shelf life'),
        ),
    ]

from django.db import migrations

def create_14_mandatory_allergens(apps, schema_editor):
    """
    There are 14 mandatory allergens, they are pre-injected
    """

    Allergen = apps.get_model("food", "allergen")
    
    Allergen.objects.get_or_create(
        name="Gluten",
    ) 
    Allergen.objects.get_or_create(
        name="Fruits à coques",
    )
    Allergen.objects.get_or_create(
        name="Crustacés",
    )
    Allergen.objects.get_or_create(
        name="Céléri",
    )
    Allergen.objects.get_or_create(
        name="Oeufs",
    )
    Allergen.objects.get_or_create(
        name="Moutarde",
    )
    Allergen.objects.get_or_create(
        name="Poissons",
    )
    Allergen.objects.get_or_create(
        name="Soja",
    )
    Allergen.objects.get_or_create(
        name="Lait",
    )
    Allergen.objects.get_or_create(
        name="Sulfites",
    )
    Allergen.objects.get_or_create(
        name="Sésame",
    )
    Allergen.objects.get_or_create(
        name="Lupin",
    )
    Allergen.objects.get_or_create(
        name="Arachides",
    )
    Allergen.objects.get_or_create(
        name="Mollusques",
    )

class Migration(migrations.Migration):
    dependencies = [
        ('food', '0002_transformedfood_shelf_life'),
    ]

    operations = [
        migrations.RunPython(create_14_mandatory_allergens),
    ]
    
    

# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path

from . import views

app_name = 'food'

urlpatterns = [
    path('', views.TransformedListView.as_view(), name='food_list'),
    path('<int:slug>', views.QRCodeView.as_view(), name='qrcode_view'),
    path('detail/<int:pk>', views.FoodView.as_view(), name='food_view'),

    path('<int:slug>/create_qrcode', views.QRCodeCreateView.as_view(), name='qrcode_create'),
    path('<int:slug>/create_qrcode/basic', views.QRCodeBasicFoodCreateView.as_view(), name='qrcode_basic_create'),
    path('create/transformed', views.TransformedFoodCreateView.as_view(), name='transformed_create'),
    path('update/basic/<int:pk>', views.BasicFoodUpdateView.as_view(), name='basic_update'),
    path('update/transformed/<int:pk>', views.TransformedFoodUpdateView.as_view(), name='transformed_update'),
    path('add/<int:pk>', views.AddIngredientView.as_view(), name='add_ingredient'),
]

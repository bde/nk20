# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from .base import WEISurvey, WEISurveyInformation, WEISurveyAlgorithm
from .wei2024 import WEISurvey2024


__all__ = [
    'WEISurvey', 'WEISurveyInformation', 'WEISurveyAlgorithm', 'CurrentSurvey',
]

CurrentSurvey = WEISurvey2024

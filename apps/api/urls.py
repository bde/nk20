# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.conf.urls import include
from django.urls import re_path
from rest_framework import routers

from .views import UserInformationView
from .viewsets import ContentTypeViewSet, UserViewSet

# Routers provide an easy way of automatically determining the URL conf.
# Register each app API router and user viewset
router = routers.DefaultRouter()
router.register('models', ContentTypeViewSet)
router.register('user', UserViewSet)

if "activity" in settings.INSTALLED_APPS:
    from activity.api.urls import register_activity_urls
    register_activity_urls(router, 'activity')

if "food" in settings.INSTALLED_APPS:
    from food.api.urls import register_food_urls
    register_food_urls(router, 'food')

if "logs" in settings.INSTALLED_APPS:
    from logs.api.urls import register_logs_urls
    register_logs_urls(router, 'logs')

if "member" in settings.INSTALLED_APPS:
    from member.api.urls import register_members_urls
    register_members_urls(router, 'members')

if "note" in settings.INSTALLED_APPS:
    from note.api.urls import register_note_urls
    register_note_urls(router, 'note')

if "permission" in settings.INSTALLED_APPS:
    from permission.api.urls import register_permission_urls
    register_permission_urls(router, 'permission')

if "treasury" in settings.INSTALLED_APPS:
    from treasury.api.urls import register_treasury_urls
    register_treasury_urls(router, 'treasury')

if "wei" in settings.INSTALLED_APPS:
    from wei.api.urls import register_wei_urls
    register_wei_urls(router, 'wei')

if "wrapped" in settings.INSTALLED_APPS:
    from wrapped.api.urls import register_wrapped_urls
    register_wrapped_urls(router, 'wrapped')

app_name = 'api'

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    re_path('^', include(router.urls)),
    re_path('^me/', UserInformationView.as_view()),
    re_path('^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

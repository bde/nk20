# Generated by Django 2.2.28 on 2024-03-21 23:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('treasury', '0007_auto_20240311_1549'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='payment_date',
            field=models.CharField(default='', max_length=255, verbose_name='Payment date'),
        ),
        migrations.AddField(
            model_name='invoice',
            name='quotation',
            field=models.BooleanField(default=False, verbose_name='Quotation'),
        ),
    ]

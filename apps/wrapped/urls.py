# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path

from . import views

app_name = 'wrapped'

urlpatterns = [
    path('', views.WrappedListView.as_view(), name='wrapped_list'),
    path('<int:pk>/', views.WrappedDetailView.as_view(), name='wrapped_detail'),
]

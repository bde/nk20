# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from api.viewsets import ReadProtectedModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter

from .serializers import WrappedSerializer, BdeSerializer
from ..models import Wrapped, Bde


class WrappedViewSet(ReadProtectedModelViewSet):
    """
    REST API View set.
    The djangorestframework plugin will get all `Wrapped` objects, serialize it to JSON with the given
    serializer, then render it on /api/wrapped/wrapped/
    """
    queryset = Wrapped.objects.order_by('id')
    serializer_class = WrappedSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['note', 'bde', ]
    search_fields = ['$note', ]


class BdeViewSet(ReadProtectedModelViewSet):
    """
    REST API View set.
    The djangorestframework plugin will get all `Bde` objects, serialize it to JSON with the given
    serializer, then render it on /api/wrapped/bde/
    """
    queryset = Bde.objects.order_by('id')
    serializer_class = BdeSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['name', ]
    search_fields = ['$name', ]

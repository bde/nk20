# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from rest_framework import serializers

from ..models import Wrapped, Bde


class WrappedSerializer(serializers.ModelSerializer):
    """
    REST API Serializer for Wrapped.
    The djangorestframework plugin will analyse the model `Wrapped` and parse all fields in the API.
    """

    class Meta:
        model = Wrapped
        fields = '__all__'


class BdeSerializer(serializers.ModelSerializer):
    """
    REST API Serializer for Bde.
    The djangorestframework plugin will analyse the model `Bde` and parse all fields in the API.
    """

    class Meta:
        model = Bde
        fields = '__all__'

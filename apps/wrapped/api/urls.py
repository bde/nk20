# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from .views import WrappedViewSet, BdeViewSet


def register_wrapped_urls(router, path):
    """
    Configure router for Wrapped REST API.
    """
    router.register(path + '/wrapped', WrappedViewSet)
    router.register(path + '/bde', BdeViewSet)

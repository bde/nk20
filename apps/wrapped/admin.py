# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin
from note_kfet.admin import admin_site

from .models import Bde, Wrapped


@admin.register(Bde, site=admin_site)
class BdeAdmin(admin.ModelAdmin):
    pass


@admin.register(Wrapped, site=admin_site)
class WrappedAdmin(admin.ModelAdmin):
    pass
